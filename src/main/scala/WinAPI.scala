/**
  * Created by Sapog on 11/23/2015.
  */
import com.sun.jna.{Library, Native, Platform}

trait Kernel32 extends Library {
  def Beep(dwFreq: Int, dwDuration: Int): Boolean
}

object WinAPI {
  def Kernel32Instance = Native.loadLibrary(
    "kernel32.dll",
    classOf[Kernel32]).asInstanceOf[Kernel32]

  //wrapper
  def beep(freq: Int, duration: Int): Boolean = Kernel32Instance.Beep(freq, duration)
}
