/**
  * Created by Sapog on 11/28/2015.
  */
object ServerProtocol {

  sealed trait Request

  case object Register extends Request

  case object Unregister extends Request

  case class Play(filename: String) extends Request

  //  case object RefStorageRequest extends Request

  sealed trait Reply

  case object RegisterSuccess extends Reply

  case object Offline extends Reply

  //  case class RefStorageReply(refs: IndexedSeq[ActorRef]) extends Reply
}
