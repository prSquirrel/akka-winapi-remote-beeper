/**
  * Created by Sapog on 11/24/2015.
  */

import BeeperProtocol._

import scala.io.Source

object BeepFileReader {
  def load(filename: String): (Int, Seq[Beep]) = {
    val src = Source.fromFile(filename).getLines

    val numOfBeepers = src.take(1).next.toInt
    val beeps = src.map(_.split(',') match {
      case Array(id, freq, dur, delay) => Beep(id.toInt, freq.toInt, dur.toInt, delay.toInt)
    }).toSeq

    (numOfBeepers, beeps)
  }
}
