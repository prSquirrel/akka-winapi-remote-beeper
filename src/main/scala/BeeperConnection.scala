/**
  * Created by Sapog on 11/28/2015.
  */

import BeeperConnection._
import BeeperProtocol._
import ServerProtocol._
import akka.actor.SupervisorStrategy.Restart
import akka.actor._

import scala.concurrent.duration._
import scala.concurrent.{Future, _}

object BeeperConnection {
  def listenUntilAvailable(actorToListen: ActorSelection,
                           scheduler: Scheduler,
                           timeout: FiniteDuration,
                           retryPeriod: FiniteDuration)(implicit executor: ExecutionContext): Future[ActorRef] = {
    val availablePromise = Promise[ActorRef]
    val pinger: Cancellable = scheduler.schedule(0.seconds, retryPeriod) {
      val f = actorToListen.resolveOne(timeout)
      f onSuccess {
        case s => availablePromise.success(s)
      }
    }

    val listenPromise = Promise[ActorRef]
    availablePromise.future onSuccess {
      case ref =>
        pinger.cancel()
        listenPromise.success(ref)
    }
    listenPromise.future
  }
}

//TODO: make ClientConnection abstract, extract BeeperConnection

class BeeperConnection extends Actor with ActorLogging {
//  override val supervisorStrategy =
//    OneForOneStrategy() {
//      //case _: ActorInitializationException => Restart
//      case _: ActorKilledException => Restart
//      case _: DeathPactException => Restart
//      //case _: Exception => Restart
//    }
  val contextConfig = context.system.settings.config
  val remotePath = contextConfig.getString("beeper.server.path")
  val server = context.actorSelection(remotePath)
  val beeper = context.actorOf(Props[Beeper], name = "beeper")

  var hasRegistered = false

  def register(): Unit = {
    import context.dispatcher
    val retryPeriod = contextConfig.getLong("beeper.server.association.retryPeriodSeconds").seconds
    val timeout = contextConfig.getLong("beeper.server.association.timeoutSeconds").seconds
    log.info("Waiting for server to become available.")
    listenUntilAvailable(server, context.system.scheduler, timeout, retryPeriod) onSuccess {
      case _ =>
        log.info("Found server, sending register message.")
        server ! Register
    }
  }

  override def preStart(): Unit = {
    register()
  }

  override def postStop(): Unit = {
    hasRegistered = false
    server ! Unregister
  }

  def waiting: Receive = {
    case RegisterSuccess =>
      hasRegistered = true
      context.watch(sender)
      log.info(s"Associated with: $sender")
      context.become(registered)
  }

  def registered: Receive = {
    case b: Beep =>
      beeper forward b
    case Offline =>
      hasRegistered = false
      context.unwatch(sender)
      log.info("Received [Offline] from server.")
      context.unbecome()
      register()
    case Terminated(s) =>
      hasRegistered = false
      log.info("Server is dead.")
      context.unbecome()
      register()
  }

  def receive = waiting
}
