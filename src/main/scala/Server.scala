/**
  * Created by Sapog on 11/23/2015.
  */

import java.io.File

import BeeperProtocol._
import ServerProtocol._
import akka.actor._
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._
import scala.io.StdIn

object Server {
  def main(args: Array[String]) {
    val configFile = getClass.getClassLoader.getResource("server_application.conf").getFile
    val config = ConfigFactory.parseFile(new File(configFile))
    val system = ActorSystem("ServerSystem", config)
    val localActor = system.actorOf(Props[Server], name = "server")

    println("quit/play?")
    while (true) {
      StdIn.readLine().toLowerCase match {
        case "q" | "quit" =>
          system.terminate()
          return
        case "p" | "play" =>
          print("Filename: ")
          localActor ! Play(StdIn.readLine)
        case "help" => println("quit/play?")
        case unknown => println(s"Unknown command: $unknown")
      }
    }
  }
}

//  val beepers = Vector.fill(num) {
//    context.actorOf(Props[Beeper])
//  }
//DONE: implement identification message sent from each Beeper to server, so that server can build a list of available beeper actors

class Server extends Actor {
  val beeperRefs = scala.collection.mutable.HashSet.empty[ActorRef]

  import context.dispatcher

  override def postStop(): Unit = {
    beeperRefs.foreach(_ ! Offline)
  }

  def receive = {
    //TODO: move this to separate method and enclose it within Try monad
    case Play(filename) =>
      val (num, beeps) = BeepFileReader.load(filename)

      val beepers = beeperRefs.toIndexedSeq
      if (beepers.nonEmpty && beepers.size >= num) {
        println("Playing...")
        println(beeperRefs)
        for (beep <- beeps) {
          context.system.scheduler.scheduleOnce(beep.delay.millis, beepers(beep.id), beep)
        }
      }
      else println("Not enough players")

    case Finished =>
      println(sender)
      println("Finished")

    case Register =>
      beeperRefs += sender
      sender ! RegisterSuccess
      context.watch(sender)

    case Unregister =>
      beeperRefs -= sender
      context.unwatch(sender)

    case Terminated(a) =>
      beeperRefs -= a

    //    case RefStorageRequest =>
    //      sender ! RefStorageReply(beeperRefs.toIndexedSeq)
  }

}
