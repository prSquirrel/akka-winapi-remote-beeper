import java.io.File

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory

/**
  * Created by Sapog on 11/28/2015.
  */
object Client {
  def main(args: Array[String]): Unit = {
    val configFile = getClass.getClassLoader.getResource("beeper_application.conf").getFile
    val config = ConfigFactory.parseFile(new File(configFile))
    val system = ActorSystem("BeeperSystem", config)
    val connection = system.actorOf(Props[BeeperConnection], name = "beeperConnection")
  }
}
