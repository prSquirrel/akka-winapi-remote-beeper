/**
  * Created by Sapog on 11/23/2015.
  */

import BeeperProtocol._
import WinAPI.beep
import akka.actor._

class Beeper extends Actor with ActorLogging {
  def receive = {
    case Beep(id, frequency, duration, delay) =>
      log.info(s"Playing #$id ${frequency}Hz for ${duration}ms, delay: ${delay}ms")
      beep(frequency, duration)
      sender() ! Finished
  }
}