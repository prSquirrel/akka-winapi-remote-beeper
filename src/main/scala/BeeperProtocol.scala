/**
  * Created by Sapog on 11/28/2015.
  */
object BeeperProtocol {

  sealed trait Request

  case class Beep(id: Int, frequency: Int, duration: Int, delay: Int) extends Request

  sealed trait Reply

  case object Finished extends Reply

}
